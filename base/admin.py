from django.contrib import admin
from .models import PhoneNumber


class PhoneNumberAdmin(admin.ModelAdmin):
	list_display = ('msisdn', 'created', 'is_rent', 'owner', 'active', 'is_top',)
	list_filter = ('is_rent', 'active', 'is_top', 'operator',)


# Register your models here.
admin.site.register(PhoneNumber, PhoneNumberAdmin)
