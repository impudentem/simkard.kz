from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from .models import *


class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']


class PhoneNumberForm(forms.ModelForm):
    class Meta:
        model = PhoneNumber
        fields = ['operator', 'msisdn', 'city', 'contact_phone', "price", 'is_rent', 'description']

    def __init__(self, *args, **kwargs):
        if "user" in kwargs:
            self.user = kwargs.pop('user')
        super(PhoneNumberForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        m = super(PhoneNumberForm, self).save(commit=False)
        if hasattr(self, "user"):
            m.owner = self.user
        if commit:
            m.save()
        return m

    def without_country_code(self, field):
        m = super(PhoneNumberForm, self).clean()
        _field = m.get(field)
        if _field and isinstance(_field, OrigPhoneNumber):
            _field = str(_field.national_number)
            return re.sub(r'^(\d{3})(\d{3})(\d{2})(\d{2})$', r'(\1) \2-\3-\4', _field)
        return ''
    # def clean(self):
    #     # run the standard clean method first
    #     cleaned_data = super(PhoneNumberForm, self).clean()
    #     msisdn = cleaned_data.get("msisdn").replace("-", "")[:10]
    #     contact_phone = cleaned_data.get("contact_phone").replace("-", "")[:10]
    #     cleaned_data['msisdn'] = msisdn
    #     cleaned_data['contact_phone'] = contact_phone
    #
    #     return cleaned_data
