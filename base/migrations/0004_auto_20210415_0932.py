# Generated by Django 3.1.7 on 2021-04-15 09:32

import base.models
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0003_auto_20210413_0833'),
    ]

    operations = [
        migrations.AlterField(
            model_name='phonenumber',
            name='contact_phone',
            field=base.models.PhoneNumberField(blank=True, max_length=128, region=None, verbose_name='Контактный телефон'),
        ),
        migrations.AlterField(
            model_name='phonenumber',
            name='msisdn',
            field=base.models.PhoneNumberField(blank=True, max_length=128, region=None, verbose_name='Номер телефона'),
        ),
    ]
