# Generated by Django 3.2a1 on 2021-03-05 19:18

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='PhoneNumber',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('operator', models.PositiveSmallIntegerField(choices=[(1, 'activ'), (2, 'kcell'), (3, 'altel'), (4, 'tele2'), (5, 'beeline'), (6, 'izi')], verbose_name='Оператор')),
                ('msisdn', models.CharField(max_length=10, verbose_name='Номер телефона')),
                ('price', models.IntegerField(verbose_name='Цена')),
                ('is_rent', models.BooleanField(default=False, verbose_name='Аренда')),
                ('city', models.PositiveSmallIntegerField(choices=[(1, 'Алматы'), (2, 'Нур-Султан'), (3, 'Шымкент')], verbose_name='Город')),
                ('description', models.CharField(max_length=200, verbose_name='Описание')),
                ('contact_phone', models.CharField(max_length=10, verbose_name='Контактный телефон')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('active', models.BooleanField(default=False)),
                ('is_top', models.BooleanField(default=False, verbose_name='Топ номер')),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Красивый номер',
                'verbose_name_plural': 'Красивые номера',
            },
        ),
    ]
